#!/bin/bash
ln -sf $HOME/.dotfiles/.bash_profile $HOME/
ln -sf $HOME/.dotfiles/.ssh/ $HOME/
ln -sf $HOME/.dotfiles/.tmux.conf $HOME/
ln -sf $HOME/.dotfiles/.vim $HOME/
ln -sf $HOME/.dotfiles/.vimrc $HOME/
ln -sf $HOME/.dotfiles/.bashrc $HOME/
