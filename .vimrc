"""""""""""""""""""""""
" => VIM User Interface
"""""""""""""""""""""""
" Ignore case when searching
set ignorecase

" Highlight search results
set hlsearch

"""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""
" Enable syntax highlighting
syntax enable
colorscheme lucius
LuciusDarkHighContrast
